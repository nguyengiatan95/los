﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebTimaCare.Models.Upload;
using WebTimaCare.Entity;
using WebTimaCare.Services;
using static WebTimaCare.Models.AI.AI;
using LOS.DAL.DTOs;
using WebTimaCare.Helpers;
using static WebTimaCare.Models.Esign.Esign;
using WebTimaCare.Models.AI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using static WebTimaCare.Models.Upload.Upload;
using static WebTimaCare.Models.LoanBrief.LoanBrief;

namespace WebTimaCare.Controllers
{

    public class HomeController : BaseController
    {
        private ILosService _losService;
        private IUploadService _uploadService;
        private IAIService _aiService;
        private ILoanBriefService _loanService;
        private IEsignService _esignService;
        private IIdentityService _identityService;

        public HomeController(ILosService losService, IUploadService uploadService, IAIService aiService, ILoanBriefService loanService, IEsignService esignService,
            IIdentityService identityService)
        {
            this._losService = losService;
            _uploadService = uploadService;
            _aiService = aiService;
            _loanService = loanService;
            _esignService = esignService;
            _identityService = identityService;
        }
        public ActionResult Index()
        {

            return View();
        }

        [Route("/upload/intro.html")]
        //[AuthorizeFilterAttribute]
        public IActionResult UploadIntro()
        {
            return View();
        }

        [Route("/otp.html")]
        public IActionResult Otp()
        {
            return View();
        }

        [Route("/upload.html")]
        [AuthorizeFilterAttribute]
        [HttpGet]
        public IActionResult Upload(int t = 0)
        {
            //Kiểm tra xem có đơn vay không
            var user = GetUserGlobal();
            var listLoan = _loanService.GetLoanByPhone(user.Token, user.Phone);
            if (listLoan == null || listLoan.Count == 0)
                return Redirect("/not-loan.html");
            ViewBag.LoanbriefId = listLoan.First().LoanBriefId;
            ViewBag.FullName = listLoan.First().FullName;
            ViewBag.NationalCard = listLoan.First().NationalCard;
            ViewBag.ReConfirm = t;
            ViewBag.TypeRemarketing = listLoan.First().TypeRemarketing.GetValueOrDefault(0);
            return View();
        }

        [HttpPost]
        [AuthorizeFilterAttribute]
        public async Task<IActionResult> UploadFile(List<IFormFile> files, int loanBriefId, int documentId, string fullName, string nationalCard)
        {
            var user = GetUserGlobal();
            var token = user.Token;

            //Verify and Upload CMND Front

            if (files != null && files.Count > 0)
            {
                for (int i = 0; i < files.Count; i++)
                {
                    var subTypeId = 0;
                    var fileBytes = new byte[0];
                    if (files[i].Length > 0)
                    {
                        using (var ms = new MemoryStream())
                        {
                            files[i].CopyTo(ms);
                            fileBytes = ms.ToArray();
                        }
                    }
                    //Call verify AI
                    var objAI = new EkycNationalIntput
                    {
                        LoanBriefId = loanBriefId,
                        FileBytes = fileBytes,
                        FileName = files[i].FileName
                    };
                    var resultAI = _aiService.EkycNationalCard(token, objAI);
                    if (resultAI != null && resultAI.Meta.ErrorCode == 200)
                    {
                        if (i == 0 && resultAI.Data.Side != "FRONT")
                            return Json(new { status = 0, message = "Hình ảnh không phải CMND/CCCD mặt trước" });
                        if (i == 1 && resultAI.Data.Side != "BACK")
                            return Json(new { status = 0, message = "Hình ảnh không phải CMND/CCCD mặt sau" });
                        if (!string.IsNullOrEmpty(resultAI.Data.Info.Id) && !string.IsNullOrEmpty(nationalCard))
                        {
                            if (String.Compare(nationalCard, resultAI.Data.Info.Id, false) != 0)
                                return Json(new { status = 0, message = "Sai số trên CMND/CCCD và đơn vay" });
                        }
                        if (!string.IsNullOrEmpty(resultAI.Data.Info.Name) && !string.IsNullOrEmpty(fullName))
                        {
                            fullName = ConvertExtensions.ConvertToUnSign(fullName.Trim().ToLower());
                            var resultName = ConvertExtensions.ConvertToUnSign(resultAI.Data.Info.Name.Trim().ToLower());
                            if (String.Compare(fullName, resultName, false) != 0)
                                return Json(new { status = 0, message = "Sai tên trên CMND/CCCD và đơn vay" });
                        }
                        if (i == 0)
                            subTypeId = 1;
                        else
                            subTypeId = 2;
                    }
                    else
                        return Json(new { status = 0, message = "Vui lòng upload lại ảnh đúng định dạng" });

                    //Upload
                    var obj = new Upload.Input
                    {
                        LoanbriefId = loanBriefId,
                        TypeDocumentId = documentId,
                        UploadFrom = (int)EnumSourceUpload.WebTimaCare,
                        FileBytes = fileBytes,
                        FileName = files[i].FileName,
                        SubTypeId = subTypeId

                    };
                    var result = _uploadService.UploadImage(token, obj);
                }


                return Json(new { status = 1, message = "Upload ảnh thành công" });

            }
            else
            {
                return Json(new { status = 0, message = "Không tồn tại file hình ảnh" });
            }
        }
        [HttpPost]
        [AuthorizeFilterAttribute]
        public async Task<IActionResult> UploadFile2(string frontFile, string backFile, int loanBriefId, int documentId, string fullName, string nationalCard)
        {
            var user = GetUserGlobal();
            var token = user.Token;

            //Verify and Upload CMND Front
            if (string.IsNullOrEmpty(frontFile) || string.IsNullOrEmpty(backFile))
                return Json(new { status = 0, message = "Vui lòng chụp đầy đủ hai mặt CMND/CCCD" });
            for (int i = 0; i < 2; i++)
            {
                var subTypeId = 0;
                var fileBytes = new byte[0];
                if (i == 0)
                    fileBytes = Convert.FromBase64String(frontFile);
                else
                    fileBytes = Convert.FromBase64String(backFile);

                //Call verify AI
                var objAI = new EkycNationalIntput
                {
                    LoanBriefId = loanBriefId,
                    FileBytes = fileBytes,
                    FileName = i == 0 ? "front-card.jpeg" : "back-card.jpeg"
                };
                var resultAI = _aiService.EkycNationalCard(token, objAI);
                if (resultAI != null && resultAI.Meta.ErrorCode == 200 && resultAI.Data != null)
                {
                    if (i == 0 && resultAI.Data.Side != "FRONT")
                        return Json(new { status = 0, message = "Hình ảnh không phải CMND/CCCD mặt trước" });
                    if (i == 1 && resultAI.Data.Side != "BACK")
                        return Json(new { status = 0, message = "Hình ảnh không phải CMND/CCCD mặt sau" });
                    if (!string.IsNullOrEmpty(resultAI.Data.Info.Id) && !string.IsNullOrEmpty(nationalCard))
                    {
                        if (String.Compare(nationalCard, resultAI.Data.Info.Id, false) != 0)
                            return Json(new { status = 0, message = "Sai số trên CMND/CCCD và đơn vay" });
                    }
                    if (!string.IsNullOrEmpty(resultAI.Data.Info.Name) && !string.IsNullOrEmpty(fullName))
                    {
                        fullName = ConvertExtensions.ConvertToUnSign(fullName.Trim().ToLower());
                        var resultName = ConvertExtensions.ConvertToUnSign(resultAI.Data.Info.Name.Trim().ToLower());
                        if (String.Compare(fullName, resultName, false) != 0)
                            return Json(new { status = 0, message = "Sai tên trên CMND/CCCD và đơn vay" });
                    }
                    if (i == 0)
                        subTypeId = 1;
                    else
                        subTypeId = 2;
                }
                else
                    return Json(new { status = 0, message = "Vui lòng chụp đúng ảnh CMND/CCCD" });

                //Upload
                var obj = new Upload.Input
                {
                    LoanbriefId = loanBriefId,
                    TypeDocumentId = documentId,
                    UploadFrom = (int)EnumSourceUpload.WebTimaCare,
                    FileBytes = fileBytes,
                    FileName = i == 0 ? "front-card.jpeg" : "back-card.jpeg",
                    SubTypeId = subTypeId

                };
                var result = _uploadService.UploadImage(token, obj);
                if (result == null || result.Meta.ErrorCode != 200)
                    return Json(new { status = 0, message = "Xảy ra lỗi khi xử lý CMND/CCCD. Vui lòng F5 và thử lại" });

            }
            return Json(new { status = 1, message = "Upload ảnh thành công" });


        }
        [HttpPost]
        public IActionResult GetOtp(string phone)
        {
            var result = _losService.GetOtp(phone);
            if (result != null && result.meta != null)
            {
                if (result.meta.errorCode == 200 && !string.IsNullOrEmpty(result.data))
                {
                    var objRequestVerifyOtp = new RequestVerifyOtp()
                    {
                        Phone = phone,
                        Otp = result.data
                    };
                    HttpContext.Session.SetString("SessionOtp", JsonConvert.SerializeObject(objRequestVerifyOtp));

                    return Json(new { status = 1 });
                }
                else
                    return Json(new { status = 0, message = result.meta.errorMessage });
            }
            else
                return Json(new { status = 0, message = "Lỗi đăng nhập. Vui lòng thử lại sau." });
        }

        [HttpPost]
        public IActionResult VerifyOtp(string otp)
        {
            var sessionOtp = JsonConvert.DeserializeObject<RequestVerifyOtp>(HttpContext.Session.GetString("SessionOtp"));
            if (sessionOtp != null)
            {
                //kiểm tra xem otp nhập có đúng không
                if (otp == sessionOtp.Otp)
                {
                    var verify = _losService.VerifyOtp(sessionOtp);
                    if (verify != null && verify.meta != null)
                    {
                        if (verify.meta.errorCode == 200 && !string.IsNullOrEmpty(verify.data))
                        {

                            sessionOtp.Token = verify.data;
                            HttpContext.Session.SetString("SessionToken", Newtonsoft.Json.JsonConvert.SerializeObject(sessionOtp));

                            //kiểm tra đã có ảnh CMND hay chưa
                            var redirectUrl = "";
                            var checkImage = _loanService.CheckVerifyImage(verify.data, sessionOtp.Phone);
                            if (checkImage)
                                redirectUrl = "/loan-info.html";
                            else
                                redirectUrl = "/upload/intro.html";

                            return Json(new { status = 1, url = redirectUrl });
                        }
                        else
                            return Json(new { status = 0, message = verify.meta.errorMessage });
                    }
                    else
                        return Json(new { status = 0, message = "Lỗi nhập OTP. Vui lòng thử lại sau." });
                }
                else
                    return Json(new { status = 0, message = "OTP không chính xác. Vui lòng kiểm tra lại." });
            }
            else
                return Json(new { status = -1, message = "Mã OTP đã hết hạn. Vui lòng lấy lại." });
        }


        [Route("not-loan.html")]
        public IActionResult CustomerNotLoan()
        {
            return View();
        }

        [Route("loan-info.html")]
        [AuthorizeFilterAttribute]
        public IActionResult GetLoanInfo()
        {

            var user = GetUserGlobal();
            var lstLoan = _loanService.GetLoanByPhone(user.Token, user.Phone);
            if (lstLoan != null && lstLoan.Count > 0)
            {
                foreach (var item in lstLoan)
                {
                    if (item.BuyInsurenceCustomer.GetValueOrDefault(false) == true || item.LoanBriefProperty.BuyInsuranceProperty.GetValueOrDefault(false) == true)
                    {
                        var obj = new InsurenceInput
                        {
                            TotalMoneyDisbursement = Convert.ToInt64(item.LoanAmount),
                            NotSchedule = 0,
                            LoanTime = Convert.ToInt32(item.LoanTime) * 30,
                            Frequency = 30,
                            RateType = Convert.ToInt64(item.RateTypeId),
                            Rate = Convert.ToDecimal(98.55)
                        };
                        var data = _loanService.GetInsurence(obj);
                        if (data != null)
                        {
                            item.FeeInsuranceOfCustomer = data.FeeInsurrance;
                            item.FeeInsuranceOfProperty = data.FeeInsuranceMaterialCovered;
                        }
                    }

                    //kiểm tra đã check ekyc chưa
                    var checkEkyc = _aiService.CheckEkyc(user.Token, item.LoanBriefId);
                    if (checkEkyc)
                        item.IsCheckEkyc = true;
                    else
                    {
                        var checkImage = _loanService.CheckVerifyImage(user.Token, item.Phone);
                        if (checkImage)
                            item.RedirectUrl = "/upload.html";
                        else
                            item.RedirectUrl = "/upload/intro.html";
                    }
                }
            }
            return View(lstLoan);
        }

        [Route("contract_financial.html")]
        public IActionResult ViewContractFinancial()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetViewContractFinancial(int loanbriefId = 0)
        {
            var data = _losService.GetContractFinancialAgreementBase64(loanbriefId);
            return Json(new { data = data });
        }

        [HttpPost]
        [AuthorizeFilterAttribute]
        public IActionResult SignContractBorrower(int loanBriefId, int typeRemarketing)
        {
            var user = GetUserGlobal();
            if (loanBriefId > 0)
            {
                //Validate Loan
                var validate = _loanService.ValidateEsign(user.Token, loanBriefId);
                if (validate.meta.errorCode != 200)
                    return Json(new { status = 0, message = validate.meta.errorMessage });
                //Kiểm tra đơn vay đã được ekyc
                if (typeRemarketing != (int)EnumTypeRemarketing.DebtRevolvingLoan && typeRemarketing != (int)EnumTypeRemarketing.IsTopUp)
                {
                    var checkEkyc = _aiService.CheckEkyc(user.Token, loanBriefId);
                    if (!checkEkyc)
                        return Json(new { status = 0, message = "Đơn vay của bạn chưa được check ekyc!" });
                }

                var obj = new SignContractInput
                {
                    LoanBriefId = loanBriefId
                };
                var result = _esignService.EsignContractBorrower(user.Token, obj);
                if (result != null)
                {
                    if (result.Meta.ErrorCode == 200)
                        return Json(new { status = 1 });
                    else
                        return Json(new { status = 0, message = result.Meta.ErrorMessage });
                }
                else
                    return Json(new { status = 0, message = "Có lỗi xảy ra trong quá trình ký HĐ. Vui lòng thử lại sau" });
            }
            else
                return Json(new { status = 0 });
        }

        [HttpPost]
        [AuthorizeFilterAttribute]
        public IActionResult ConfirmEsignContractBorrower(int loanBriefId, string otp)
        {
            var user = GetUserGlobal();
            if (loanBriefId > 0 && !string.IsNullOrEmpty(otp))
            {
                var obj = new ConfirmSignContractInput
                {
                    LoanBriefId = loanBriefId,
                    Otp = otp
                };
                var result = _esignService.ConfirmEsignContractBorrower(user.Token, obj);
                if (result != null)
                {
                    if (result.Meta.ErrorCode == 200)
                        return Json(new { status = 1, data = "http://" + result.Data });
                    else
                        return Json(new { status = 0, message = result.Meta.ErrorMessage });

                }
                else
                    return Json(new { status = 0, message = "Có lỗi xảy ra trong quá trình ký HĐ. Vui lòng thử lại sau" });
            }
            else
                return Json(new { status = 0 });
        }

        [AuthorizeFilterAttribute]
        public IActionResult CheckLiveness([FromBody] CheckLivenessReq req)
        {
            var user = GetUserGlobal();
            //Kiểm tra số lượng ảnh trước khi gọi api
            if (string.IsNullOrEmpty(req.selfie))
                return Json(new { status = 0, message = "Chưa có ảnh selfie. Vui lòng chụp ảnh selfie trước." });
            if (req.files == null || req.files.Count < 20)
                return Json(new { status = 0, message = "Video chưa đủ thời gian. Vui lòng quay lại video dài hơn." });

            var result = _aiService.EkycLiveless(user.Token, req);
            if (result != null && result.meta != null)
            {
                if (result.meta.errorCode == 200)
                {
                    if ((bool)result.data)
                        return Json(new { status = 1 });
                    else
                        return Json(new { status = -1 });
                }
                else
                    return Json(new { status = 0, message = result.meta.errorMessage });
            }
            else
                return Json(new { status = 0, message = result.meta.errorMessage });
        }

        [AuthorizeFilterAttribute]
        public IActionResult RecoveOTPSignContract(int customerId)
        {
            var user = GetUserGlobal();
            var result = _losService.RecoveOTPSignContract(user.Token, customerId);
            if (result != null)
            {
                if (result.meta != null && result.meta.errorCode == 200)
                    return Json(new { status = 1, message = "Lấy lại OTP thành công" });
                else
                    return Json(new { status = 0, message = result.meta.errorMessage });
            }
            return Json(new { status = 0, message = "Lỗi hệ thống. VUi lòng thử lại sau" });
        }

        public IActionResult ReGetOtp()
        {
            var sessionOtp = JsonConvert.DeserializeObject<RequestVerifyOtp>(HttpContext.Session.GetString("SessionOtp"));
            if (sessionOtp != null && sessionOtp.Phone != null)
            {
                var result = _losService.ReGetOtp(sessionOtp.Phone);
                if (result != null && result.meta != null)
                {
                    if (result.meta.errorCode == 200 && !string.IsNullOrEmpty(result.data))
                    {
                        sessionOtp.Otp = result.data;
                        HttpContext.Session.SetString("SessionOtp", JsonConvert.SerializeObject(sessionOtp));
                        return Json(new { status = 1, message = "Lấy lại OTP thành công!" });
                    }
                      
                    else
                        return Json(new { status = 0, message = result.meta.errorMessage });
                }
                else
                    return Json(new { status = 0, message = "Lỗi xảy ra trong quá trình lấy lại OTP. Vui lòng thử lại sau." });
            }
            else
                return Json(new { status = 0, message = "Lỗi xảy ra trong quá trình lấy lại OTP. Vui lòng thử lại sau." });

        }


    }
}