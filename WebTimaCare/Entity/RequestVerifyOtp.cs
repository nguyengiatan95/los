﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTimaCare.Entity
{
    public class RequestVerifyOtp
    {
        public string Phone { get; set; }
        public string Otp { get; set; }
        public string Token { get; set; }
    }
}
