﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTimaCare.Entity
{
    public class ResponseDefaultLOS
    {
        public Meta meta { get; set; }
        public string data { get; set; }
    }
    public class Meta
    {
        public int errorCode { get; set; }
        public string errorMessage { get; set; }
    }
}
