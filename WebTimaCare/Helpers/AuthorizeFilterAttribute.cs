﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebTimaCare.Entity;

namespace WebTimaCare.Helpers
{
	public class AuthorizeFilterAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			var session = filterContext.HttpContext.Session;
            if (session == null || session.GetObjectFromJson<RequestVerifyOtp>("SessionToken") == null || session.GetObjectFromJson<RequestVerifyOtp>("SessionToken").Result == null || session.GetObjectFromJson<RequestVerifyOtp>("SessionToken").Result.Token == null)
            {
                var _baseConfig = filterContext.HttpContext.RequestServices.GetService<IConfiguration>();
                filterContext.Result = new RedirectResult("/");
                //filterContext.Result = new RedirectToActionResult("Index", "Login", null);
            }
        }
	}
}
