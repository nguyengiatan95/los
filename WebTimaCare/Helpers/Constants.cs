﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTimaCare.Helpers
{
    public class Constants
    {
        public static string CONSTANTS_VERSION_JS = DateTime.Now.Ticks.ToString();

        public static string GetOtp = "security/get_otp?phone={0}";
        public static string ReGetOtp = "security/reget_otp?phone={0}";
        public static string Upload_Image = "file/upload_image";
        public static string AI_EKYC_NATIONAL_CARD = "lendingonline/ekyc_national_card?loanbriefId={0}";
        public static string AI_EKYC_CHECK_LIVENESS = "timacare/CheckLivenessBase64";
        public static string VerifyOtp = "security/verify_otp";
        public static string GetContractFinancialAgreementBase64 = "timacare/get_contract_financial_agreement_base64?loanbriefId={0}";
        public static string GET_LOAN_BY_PHONE = "timacare/get_loan_by_phone?phone={0}";
        public static string SIGN_CONTRACT_BORROWER = "esign/SignContractForBorrower";
        public static string CONFIRM_SIGN_CONTRACT_BORROWER = "esign/ConfirmSignContractForBorrower";
        public static string GET_LOAN_BY_ID = "timacare/loan_info?loanbriefId={0}";
        public static string CHECK_EKYC_LOAN = "timacare/check_ekyc_ai?loanBriefId={0}";
        public static string CHECK_VERIFY_IMAGE = "timacare/check_verify_image?phone={0}";
        public static string VALIDATE_ESIGN = "timacare/check_validate_esign?loanBriefId={0}";
        public static string RecoveOTPSignContract = "timacare/recove_otp_sign_contract?customerId={0}";

        public static string LMS_GET_INSURENCE = "/api/s2s/GetPaymentSchedulePresume";
    }
}
