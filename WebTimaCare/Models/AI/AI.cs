﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTimaCare.Models.AI
{
    public class AI
    {
        public class EkycNationalIntput
        {
            public int LoanBriefId { get; set; }
            public byte[] FileBytes { get; set; }
            public string FileName { get; set; }
        }
        public class EkycNationalOutput
        {
            [JsonProperty("meta")]
            public Meta Meta { get; set; }

            [JsonProperty("data")]
            public Data Data { get; set; }
        }
        public partial class Data
        {
            [JsonProperty("side")]
            public string Side { get; set; }
            [JsonProperty("info")]
            public Info Info { get; set; }
        }

        public partial class Info
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

        }
        public partial class Meta
        {
            [JsonProperty("errorCode")]
            public long ErrorCode { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrorMessage { get; set; }
        }
    }
}
