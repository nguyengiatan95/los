﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTimaCare.Models.AI
{
	public class CheckLivenessReq
	{
		public string selfie { get; set; }
		public List<string> files { get; set; }
		public int loanBriefId { get; set; }
	}
}
