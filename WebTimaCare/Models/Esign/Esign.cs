﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTimaCare.Models.Esign
{
    public class Esign
    {
        public partial class SignContractInput
        {
            [JsonProperty("loanBriefId")]
            public int LoanBriefId { get; set; }
        }

        public partial class ConfirmSignContractInput
        {
            [JsonProperty("loanBriefId")]
            public int LoanBriefId { get; set; }
            [JsonProperty("otp")]
            public string Otp { get; set; }
        }
        public partial class SignContractOutput
        {
            [JsonProperty("meta")]
            public Meta Meta { get; set; }

            [JsonProperty("data")]
            public string Data { get; set; }
        }

        public partial class Meta
        {
            [JsonProperty("errorCode")]
            public long ErrorCode { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrorMessage { get; set; }
        }
    }
}
