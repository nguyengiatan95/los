﻿using LOS.DAL.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTimaCare.Models.LoanBrief
{
    public class LoanBrief
    {
        public partial class GetLoanOutPut
        {
            [JsonProperty("meta")]
            public Meta Meta { get; set; }

            [JsonProperty("data")]
            public List<LoanBriefTimaCareDTO> Data { get; set; }
        }

        public partial class GetLoanInfoRes
        {
            [JsonProperty("meta")]
            public Meta Meta { get; set; }

            [JsonProperty("data")]
            public LoanBriefTimaCareDTO Data { get; set; }
        }

        public partial class Meta
        {
            [JsonProperty("errorCode")]
            public long ErrorCode { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrorMessage { get; set; }
        }

        public partial class InsurenceInput
        {
            [JsonProperty("TotalMoneyDisbursement")]
            public long TotalMoneyDisbursement { get; set; }

            [JsonProperty("LoanTime")]
            public long LoanTime { get; set; }

            [JsonProperty("RateType")]
            public long RateType { get; set; }

            [JsonProperty("Frequency")]
            public long Frequency { get; set; }

            [JsonProperty("NotSchedule")]
            public int NotSchedule { get; set; }


            [JsonProperty("Rate")]
            public decimal Rate { get; set; }
        }

        public partial class InsurenceOutput
        {
            [JsonProperty("Status")]
            public long Result { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("Data")]
            public DataInsurence Data { get; set; }
        }

        public partial class DataInsurence
        {

            [JsonProperty("FeeInsurrance")]
            public long FeeInsurrance { get; set; }

            [JsonProperty("FeeInsuranceMaterialCovered")]
            public long FeeInsuranceMaterialCovered { get; set; }
        }
    }
}
