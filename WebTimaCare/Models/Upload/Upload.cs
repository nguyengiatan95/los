﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTimaCare.Models.Upload
{
    public class Upload
    {
        public partial class Input
        {
            public int LoanbriefId { get; set; }
            public int TypeDocumentId { get; set; }
            public int UploadFrom { get; set; }
            public int SubTypeId { get; set; }
            public byte [] FileBytes { get; set; }
            public string FileName { get; set; }
        }
        public partial class OutPut
        {
            [JsonProperty("meta")]
            public Meta Meta { get; set; }

            [JsonProperty("data")]
            public string Data { get; set; }
        }

        public partial class Meta
        {
            [JsonProperty("errorCode")]
            public long ErrorCode { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrorMessage { get; set; }
        }

        public class UploadRes
        {
            public int LoanBriefId { get; set; }
            public int DocumentId { get; set; }
            public List<IFormFile> Files { get; set; }
        }
    }
}
