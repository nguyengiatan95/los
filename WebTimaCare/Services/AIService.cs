﻿using LOS.Common.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebTimaCare.Entity;
using WebTimaCare.Helpers;
using WebTimaCare.Models.AI;
using static WebTimaCare.Models.AI.AI;

namespace WebTimaCare.Services
{
    public interface IAIService
    {
        EkycNationalOutput EkycNationalCard(string token, EkycNationalIntput req);
        DefaultResponse<object> EkycLiveless(string token, CheckLivenessReq req);

        bool CheckEkyc(string token, int loanBriefId);
    }
    public class AIService : IAIService
    {
        private IConfiguration _configuration;

        public AIService(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public bool CheckEkyc(string token, int loanBriefId)
        {
            try
            {
                var url = _configuration["AppSettings:UrlApi"] + String.Format(Constants.CHECK_EKYC_LOAN, loanBriefId);
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                //request.AddHeader("Authorization", $"Bearer {token}");
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseDefaultLOS>(response.Content);
                    if (result != null && result.meta != null && result.meta.errorCode == 200)
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public DefaultResponse<object> EkycLiveless(string token, CheckLivenessReq req)
        {
            try
            {
                var url = _configuration["AppSettings:UrlApi"] + Constants.AI_EKYC_CHECK_LIVENESS;
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + token);
                request.AddJsonBody(req);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var data = Newtonsoft.Json.JsonConvert.DeserializeObject<DefaultResponse<object>>(response.Content);
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public EkycNationalOutput EkycNationalCard(string token, EkycNationalIntput req)
        {
            try
            {
                var url = _configuration["AppSettings:UrlApi"] + String.Format(Constants.AI_EKYC_NATIONAL_CARD, req.LoanBriefId);
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + token);
                request.AddFile("image", req.FileBytes, req.FileName);

                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var data = Newtonsoft.Json.JsonConvert.DeserializeObject<EkycNationalOutput>(response.Content);
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
    }
}
