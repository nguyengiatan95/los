﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebTimaCare.Entity;
using WebTimaCare.Helpers;
using static WebTimaCare.Models.AI.AI;
using static WebTimaCare.Models.Esign.Esign;

namespace WebTimaCare.Services
{
    public interface IEsignService
    {
        SignContractOutput EsignContractBorrower(string token, SignContractInput entity);
        SignContractOutput ConfirmEsignContractBorrower(string token, ConfirmSignContractInput entity);
    }
    public class EsignService : IEsignService
    {
        private IConfiguration _configuration;

        public EsignService(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public SignContractOutput EsignContractBorrower(string token, SignContractInput entity)
        {
            try
            {
                var body = JsonConvert.SerializeObject(entity);
                var url = _configuration["AppSettings:UrlApi"] + Constants.SIGN_CONTRACT_BORROWER;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + token);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var data = Newtonsoft.Json.JsonConvert.DeserializeObject<SignContractOutput>(response.Content);
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public SignContractOutput ConfirmEsignContractBorrower(string token, ConfirmSignContractInput entity)
        {
            try
            {
                var body = JsonConvert.SerializeObject(entity);
                var url = _configuration["AppSettings:UrlApi"] + Constants.CONFIRM_SIGN_CONTRACT_BORROWER;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + token);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var data = Newtonsoft.Json.JsonConvert.DeserializeObject<SignContractOutput>(response.Content);
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
    }
}
