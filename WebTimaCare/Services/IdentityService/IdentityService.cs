﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebTimaCare.Services
{
    public interface IIdentityService
    {
        int GetUserIdentity();
        string GetPhoneIdentity();
        string GetTokenIdentity();
    }
    public class IdentityService: IIdentityService
    {
        private IHttpContextAccessor _context;

        public IdentityService(IHttpContextAccessor context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public int GetUserIdentity()
        {
            try
            {
                var identity = _context.HttpContext.User.Identity as ClaimsIdentity;
                return int.Parse(identity.Claims.Where(c => c.Type == "Id").Select(c => c.Value).SingleOrDefault());
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        public string GetPhoneIdentity()
        {
            try
            {
                var identity = _context.HttpContext.User.Identity as ClaimsIdentity;
                return identity.Claims.Where(c => c.Type == "phone").Select(c => c.Value).SingleOrDefault();
            }
            catch (Exception ex)
            {
            }
            return string.Empty;
        }    
        public string GetTokenIdentity()
        {
            try
            {
                StringValues _token;
                var authHeader = _context.HttpContext.Request.Headers.TryGetValue("Authorization", out _token);
                if (authHeader)
                {
                    var token = _token.ToString();
                    if (token.StartsWith("Bearer"))
                        token = token.Substring("Bearer ".Length).Trim();
                    return _token;
                }
            }
            catch (Exception ex)
            {
            }
            return string.Empty;
        }
    }
}
