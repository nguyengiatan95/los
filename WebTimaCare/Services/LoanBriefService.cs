﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebTimaCare.Entity;
using WebTimaCare.Helpers;
using LOS.DAL.DTOs;
using static WebTimaCare.Models.LoanBrief.LoanBrief;
using Newtonsoft.Json;

namespace WebTimaCare.Services
{
    public interface ILoanBriefService
    {
        List<LoanBriefTimaCareDTO> GetLoanByPhone(string token, string phone);
        LoanBriefTimaCareDTO GetLoanById(string token, int loanbriefId);
        bool CheckVerifyImage(string token, string phone);

        DataInsurence GetInsurence(InsurenceInput req);
        ResponseDefaultLOS ValidateEsign(string token, int loanBriefId);
    }
    public class LoanBriefService : ILoanBriefService
    {
        private IConfiguration _configuration;

        public LoanBriefService(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public List<LoanBriefTimaCareDTO> GetLoanByPhone(string token, string phone)
        {
            try
            {
                var url = _configuration["AppSettings:UrlApi"] + String.Format(Constants.GET_LOAN_BY_PHONE, phone);
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", $"Bearer {token}");
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<GetLoanOutPut>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                        return result.Data;
                }
                return null;
            }
            catch (Exception ex)
            {

                return null;
            }

        }

        public LoanBriefTimaCareDTO GetLoanById(string token, int loanbriefId)
        {
            try
            {
                var url = _configuration["AppSettings:UrlApi"] + String.Format(Constants.GET_LOAN_BY_ID, loanbriefId);
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", $"Bearer {token}");
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<GetLoanInfoRes>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                        return result.Data;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public bool CheckVerifyImage(string token, string phone)
        {
            try
            {
                var url = _configuration["AppSettings:UrlApi"] + String.Format(Constants.CHECK_VERIFY_IMAGE, phone);
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                //request.AddHeader("Authorization", $"Bearer {token}");
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseDefaultLOS>(response.Content);
                    if (result != null && result.meta != null && result.meta.errorCode == 200)
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public DataInsurence GetInsurence(InsurenceInput req)
        {
            try
            {
                var url = _configuration["AppSettings:LMS"] + Constants.LMS_GET_INSURENCE;
                var client = new RestClient(url);
                client.Timeout = -1;
                var body = JsonConvert.SerializeObject(req);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<InsurenceOutput>(response.Content);
                    if (result != null && result.Result == 1)
                        return result.Data;
                }
                return null;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public ResponseDefaultLOS ValidateEsign(string token, int loanBriefId)
        {
            try
            {
                var url = _configuration["AppSettings:UrlApi"] + String.Format(Constants.VALIDATE_ESIGN, loanBriefId);
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                //request.AddHeader("Authorization", $"Bearer {token}");
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseDefaultLOS>(response.Content);
                        return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
