﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebTimaCare.Entity;
using WebTimaCare.Helpers;

namespace WebTimaCare.Services
{
    public interface ILosService
    {
        ResponseDefaultLOS GetOtp(string phone);
        ResponseDefaultLOS ReGetOtp(string phone);

        ResponseDefaultLOS VerifyOtp(RequestVerifyOtp entity);

        ResponseDefaultLOS GetContractFinancialAgreementBase64(int loanbriefId);

        ResponseDefaultLOS RecoveOTPSignContract(string token, int customerId);
    }
    public class LosService : ILosService
    {
        private IConfiguration _configuration;

        public LosService(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public ResponseDefaultLOS GetOtp(string phone)
        {
            var url = _configuration["AppSettings:UrlApi"] + string.Format(Constants.GetOtp, phone);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            try
            {
                var response = client.Execute<ResponseDefaultLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseDefaultLOS>(response.Content);
                    return objData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LosService/GetOtp Exception");
                return null;
            }
        }

        public ResponseDefaultLOS ReGetOtp(string phone)
        {
            var url = _configuration["AppSettings:UrlApi"] + string.Format(Constants.ReGetOtp, phone);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            try
            {
                var response = client.Execute<ResponseDefaultLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseDefaultLOS>(response.Content);
                    return objData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LosService/ReGetOtp Exception");
                return null;
            }
        }

        public ResponseDefaultLOS VerifyOtp(RequestVerifyOtp entity)
        {
            var url = _configuration["AppSettings:UrlApi"] + Constants.VerifyOtp;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(entity);
            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                var response = client.Execute<ResponseDefaultLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseDefaultLOS>(response.Content);
                    return objData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LosService/VerifyOtp Exception");
                return null;
            }
        }

        public ResponseDefaultLOS GetContractFinancialAgreementBase64(int loanbriefId)
        {
            var url = _configuration["AppSettings:UrlGeneratePdf"] + string.Format(Constants.GetContractFinancialAgreementBase64, loanbriefId);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            try
            {
                var response = client.Execute<ResponseDefaultLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseDefaultLOS>(response.Content);
                    return objData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LosService/GetContractFinancialAgreementBase64 Exception");
                return null;
            }
        }

        public ResponseDefaultLOS RecoveOTPSignContract(string token, int customerId)
        {
            var url = _configuration["AppSettings:UrlApi"] + string.Format(Constants.RecoveOTPSignContract, customerId);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", $"Bearer {token}");
            try
            {
                var response = client.Execute<ResponseDefaultLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseDefaultLOS>(response.Content);
                    return objData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LosService/RecoveOTPSignContract Exception");
                return null;
            }
        }
    }
}
