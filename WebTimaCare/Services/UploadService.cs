﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebTimaCare.Entity;
using WebTimaCare.Helpers;
using WebTimaCare.Models.Upload;

namespace WebTimaCare.Services
{
    public interface IUploadService
    {
        Upload.OutPut UploadImage(string token, Upload.Input req);
    }
    public class UploadService : IUploadService
    {
        private IConfiguration _configuration;

        public UploadService(IConfiguration configuration)
        {
            this._configuration = configuration;
        }


        public Upload.OutPut UploadImage(string token, Upload.Input req)
        {
            try
            {
                var url = _configuration["AppSettings:UrlApi"] + Constants.Upload_Image;
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + token);
                request.AddFile("images", req.FileBytes, req.FileName);
                request.AddParameter("LoanbriefId", req.LoanbriefId);
                request.AddParameter("TypeDocumentId", req.TypeDocumentId);
                request.AddParameter("SubTypeId", req.SubTypeId);
                request.AddParameter("UploadFrom", req.UploadFrom);
                IRestResponse response = client.Execute(request);
                if(response.StatusCode == HttpStatusCode.OK)
                {
                    var data = Newtonsoft.Json.JsonConvert.DeserializeObject<Upload.OutPut>(response.Content);
                    return data;
                }
                return null;

            }
            catch (Exception ex)
            {
                return null;

            }

        }
    }
}
