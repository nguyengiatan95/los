﻿var App = new function () {
    var NotyA = function (sText, typeMes, iTime) {
        iTime = typeof iTime !== 'undefined' ? iTime : 2000;
        noty({
            text: sText,
            type: typeMes, //'success',// error
            layout: 'top',
            timeout: iTime,
            modal: false
        });
    }

    var DisplayError = function (sText) {
        NotyA(sText, 'error');
    }

    var DisplayError = function (sText, iTime) {
        NotyA(sText, 'error', iTime);
    }

    var DisplaySuccess = function (sText) {
        NotyA(sText, 'success');
    }

    return {
        NotyA: NotyA,
        DisplayError: DisplayError,
        DisplaySuccess: DisplaySuccess
    }
}
