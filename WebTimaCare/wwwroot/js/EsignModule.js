﻿var EsignModule = new function () {
    var loanId = 0;
    var SignContractBorrower = function (e, loanBriefId, typeRemarketing) {
        $(e).attr('disabled', 'disabled');
        $(e).attr('style', 'cursor: not-allowed;');
        $('#load-' + loanBriefId).addClass("fa fa-spinner fa-spin");
        $.ajax({
            type: "POST",
            url: "/Home/SignContractBorrower",
            data: { loanBriefId: loanBriefId, typeRemarketing: typeRemarketing },
            success: function (data) {
                $('#load-' + loanBriefId).removeClass("fa fa-spinner fa-spin");
                $(e).removeAttr('disabled', 'disabled');
                $(e).removeAttr('style', 'cursor: not-allowed;');
                if (data.status == 1) {
                    loanId = loanBriefId;
                    $('#m_modal_otp').modal('show');
                    //tính thời gian
                    CaculatorTime($('#txt_countdown_time').text());
                } else {
                    App.DisplayError(data.message);
                }
            }
        });
    }


    var ConfirmSignContractBorrower = function (e) {
        var loanBriefId = loanId;
        var otp = $('#confirm-otp').val();
        if (otp == undefined || otp == '') {
            App.DisplayError('Bạn phải nhập otp để hoàn thành ký hợp đồng');
            return;
        }
        $(e).attr('disabled', 'disabled');
        $(e).attr('style', 'cursor: not-allowed;margin-top: 0px !important;');
        $.ajax({
            type: "POST",
            url: "/Home/ConfirmEsignContractBorrower",
            data: { loanBriefId: loanBriefId, otp: otp },
            success: function (data) {
                $(e).removeAttr('disabled', 'disabled');
                $(e).removeAttr('style', 'cursor: not-allowed;');
                $(e).attr('style', 'margin-top: 0px !important;');
                if (data.status == 1) {
                    console.log(data.data);
                    App.DisplaySuccess("Ký HĐ thành công");
                    //Hiển thị HĐ
                    window.open(data.data, '_blank');
                    window.location.href = '/loan-info.html';

                } else {
                    App.DisplayError(data.message);
                }
            }
        });
    }

    var CreateContractBorrower = function (e, loanBriefId) {

    }

    var CountdownRecoveOTP = function (duration) {
        var timer = duration, minutes, seconds;
        var countDown = setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            $('#txt_countdown_time').text(minutes + ":" + seconds);
            if (--timer == 0) {
                $('#_timer').attr('style', 'display: none;');
                $('#btn_retype_otp').removeClass('btn-retype-opt-disabled');
                $('#btn_retype_otp').addClass('btn-retype-opt');
                $('#btn_retype_otp').removeAttr('disabled', 'disabled');
                clearInterval(countDown);
            }
        }, 1000);
    }

    var CaculatorTime = function (_timer) {
        if (_timer == "03:00") {
            var minutes = parseInt(_timer.substring(0, 2));
            var seconds = parseInt(_timer.substring(3, 5));
            var duration = parseInt((minutes * 60) + seconds);
            $('#_timer').removeAttr('style', 'display: none;');
            $('#btn_retype_otp').addClass('btn-retype-opt-disabled');
            $('#btn_retype_otp').removeClass('btn-retype-opt');
            $('#btn_retype_otp').attr('disabled', 'disabled');
            CountdownRecoveOTP(duration);
        }
    }

    var RecoveOTP = function () {
        $('#txt_countdown_time').text('03:00');
        CaculatorTime($('#txt_countdown_time').text());
        var customerId = $('#hdd_CustomerId').val();
        $.ajax({
            type: "GET",
            url: "/Home/RecoveOTPSignContract",
            data: { customerId: customerId },
            success: function (data) {
                if (data.status == 1) {
                    App.DisplaySuccess(data.message);
                } else {
                    App.DisplayError(data.message);
                }
            }
        });
    }

    return {
        SignContractBorrower: SignContractBorrower,
        ConfirmSignContractBorrower: ConfirmSignContractBorrower,
        CreateContractBorrower: CreateContractBorrower,
        CountdownRecoveOTP: CountdownRecoveOTP,
        CaculatorTime: CaculatorTime,
        RecoveOTP: RecoveOTP,
    }
}

