﻿var stateRecord = 0;
var hasVideo = 0;
var statePlay = 0;
var stateUploading = 0;
var images;
var selfieImage;
var refreshIntervalId;
var base64Selected;

const video = document.getElementById("video");
const selfie = document.getElementById("selfie");
//var fileInput = document.querySelector("#video-file");
var takePicture = 0;

$(document).ready(function () {

	//fileInput.addEventListener("change", function (event) {
	//	//$("#file-container").css("display", "block");		
	//	$("#video").css("display", "block");
	//	var src = fileInput.files[0];
	//	video.setAttribute("src", URL.createObjectURL(src));
	//	video.load();
	//	$("#step3-button-video-container").css("display", "none");
	//	$("#step3-upload-video-container").css("display", "block");
	//});

	//video.addEventListener('play', event => {
	//	// Tự động capture ngay khi video vừa play > lấy 20 ảnh
	//	// Nếu upload file thì mới record						
	//	if (fileInput.files.length > 0 && stateRecord == 0) {
	//		images = new Array();
	//		var d = 20;
	//		if (video.duration <= 10)
	//			d = 20
	//		else if (video.duration > 10 && video.duration > 12)
	//			d = 30;
	//		else if (video.duration >= 12 && video.duration > 15)
	//			d = 40;
	//		else
	//			d = 50;
	//		var interval = video.duration / d;
	//		refreshIntervalId = setInterval(function () {
	//			// Capture Image
	//			var canvas = document.createElement("canvas")
	//			canvas.width = video.videoWidth;
	//			canvas.height = video.videoHeight;
	//			canvas.getContext("2d").drawImage(video, 0, 0);
	//			// Other browsers will fall back to image/png
	//			var image = canvas.toDataURL("image/jpeg");
	//			images.push(image);				
	//			if (images.length == d) {
	//				clearInterval(refreshIntervalId);
	//			}
	//		}, interval * 1000);
	//	}
	//});

	video.addEventListener('pause', event => {
		clearInterval(refreshIntervalId);
	});

	video.addEventListener('ended', event => {
		// Tự động capture ngay khi video vừa play > lấy 20 ảnh
		// Nếu upload file thì mới record						
		$("input").removeAttr('disabled');
		statePlay = 0;
	});

	if (hasGetUserMedia()) {
		hasVideo = 1;
	} else {
		App.DisplayError("Vui lòng kiểm tra lại kết nối camera!");
	}

	$("#selfie-capture").click(function () {
		if (hasVideo == 1) {
			const constraints = {
				video: true,
			};

			navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
				$("#selfie").css("display", "block");
				$("#step2-button-video-container").css("display", "block");
				selfie.srcObject = stream;
			}).catch(function () {
				App.DisplayError("Máy tính của bạn không được trang bị thiết bị quay video, vui lòng tải lên video!");
			});
		}
	});



	$("#video-capture").click(function () {
		if (stateUploading == 0) {
			$("#file-container").css("display", "none");
			if (hasVideo == 1) {
				const constraints = {
					video: true,
				};

				navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
					$("#video").css("display", "block");
					$("#step3-upload-video-container").css("display", "none");
					$("#step3-button-video-container").css("display", "block");
					video.srcObject = stream;
				}).catch(function () {
					App.DisplayError("Máy tính của bạn không được trang bị thiết bị quay video, vui lòng tải lên video!");
				});
			}
		}
	});

	$("#take-picture").click(function () {
		if (takePicture == 0) {
			selfie.pause();
			// Capture Image		
			var canvas = document.createElement("canvas")
			canvas.width = selfie.videoWidth;
			canvas.height = selfie.videoHeight;
			canvas.getContext("2d").drawImage(selfie, 0, 0);
			// Other browsers will fall back to image/png
			selfieImage = canvas.toDataURL("image/jpeg");
			$(this).html("Chụp lại");
			takePicture = 1;
		}
		else {
			selfie.play();
			$(this).html("Chụp ảnh");
			selfieImage = undefined;
			takePicture = 0;
		}
	});

	$("#step2-next").click(function () {
		if (selfieImage == undefined) {
			App.DisplayError("Vui lòng chụp ảnh chân dung để chuyển sang bước tiếp theo.");
			return;
		}
		// Thành công chuyển sang màn tiếp theo
		var active = $('.wizard .nav-tabs li.active');
		active.next().removeClass('disabled');
		$(active).next().find('a[data-toggle="tab"]').click();
	});

	$("#btn-begin-capture").click(function () {
		if (stateUploading == 0) {
			if (stateRecord == 0) {
				images = new Array();
				stateRecord = 1;
				$(this).html("Dừng quay <i class='fa fa-video-camera fa-blink'></i>");
				refreshIntervalId = setInterval(function () {
					// Capture Image
					var canvas = document.createElement("canvas")
					canvas.width = video.videoWidth;
					canvas.height = video.videoHeight;
					canvas.getContext("2d").drawImage(video, 0, 0);
					// Other browsers will fall back to image/png
					var image = canvas.toDataURL("image/jpeg");
					images.push(image);
				}, 400);
			}
			else {
				this.innerHTML = "Bắt đầu quay";
				stateRecord = 0;
				clearInterval(refreshIntervalId);
			}
		}
	});

	$("#btn-finish-capture").click(function () {
		$("#btn-finish-capture").attr("disabled", "disabled");
		$("#capture-loading").addClass("fa fa-spinner fa-spin");
		stateUploading = 1;
		if (stateRecord == 1) {
			App.DisplayError("Vui lòng bấm dừng quay trước!");
			$("#btn-finish-capture").removeAttr('disabled');
			$("#capture-loading").removeClass("fa fa-spinner fa-spin");
			stateUploading = 0;
			return;
		}
		else {
			if (selfieImage == undefined) {
				App.DisplayError("Vui lòng chọn anh chân dung");
				$("#btn-finish-capture").removeAttr('disabled');
				$("#capture-loading").removeClass("fa fa-spinner fa-spin");
				stateUploading = 0;
				return;
			}
			if (images == undefined || images.length < 20) {
				App.DisplayError("Vui lòng quay video dài hơn!");
				$("#btn-finish-capture").removeAttr('disabled');
				$("#capture-loading").removeClass("fa fa-spinner fa-spin");
				stateUploading = 0;
				return;
			}
			selfieImage = selfieImage.replace("data:image/jpeg;base64,", "");
            var arrayImages = new Array();
            for (var i = 0; i < images.length; i++) {
                var imageBase64 = images[i];
                if (imageBase64 != "" && imageBase64 != undefined && imageBase64 != null) {
                    arrayImages.push(imageBase64.replace("data:image/jpeg;base64,", ""));
                    if (arrayImages.length >= 25) {
                        break;
                    }
                }				
			}
			var loanBriefId = parseInt($('#loanBriefId').val());
			var data = {
				selfie: selfieImage,
				files: arrayImages,
				loanBriefId: loanBriefId
            };

			$.ajax({
				type: "POST",
				url: "/Home/CheckLiveness",
				data: JSON.stringify(data),
				dataType: "json",
				contentType: "application/json; charset=utf-8"
			}).done(function (data) {
				if (data != undefined) {
					if (data.status == 1) {
						// Thành công chuyển sang màn tiếp theo
						var active = $('.wizard .nav-tabs li.active');
						active.next().removeClass('disabled');
						$(active).next().find('a[data-toggle="tab"]').click();
					}
					else if (data.status == -1) {
						// Thất bại vui lòng thực hiện lại
						App.DisplayError("Xác thực thất bại vui lòng quay lại video hoặc thay đổi ảnh chân dung!", 5000);
						$("#btn-finish-capture").removeAttr('disabled');
						$("#capture-loading").removeClass("fa fa-spinner fa-spin");
						stateUploading = 0;
					}
					else {
						App.DisplayError(data.message, 5000);
						$("#btn-finish-capture").removeAttr('disabled');
						$("#capture-loading").removeClass("fa fa-spinner fa-spin");
						stateUploading = 0;
					}
				}
				else {
					App.DisplayError("(513) Lỗi trong quá trình xử lý, vui lòng thực hiện lại sau.", 5000);
					$("#btn-finish-capture").removeAttr('disabled');
					$("#capture-loading").removeClass("fa fa-spinner fa-spin");
					stateUploading = 0;
				}
			}).fail(function () {
				App.DisplayError("(514) Lỗi trong quá trình xử lý, vui lòng thực hiện lại sau.", 5000);
				$("#btn-finish-capture").removeAttr('disabled');
				$("#capture-loading").removeClass("fa fa-spinner fa-spin");
				stateUploading = 0;
			});
		}
	});

	//$("#btn-upload-video").click(function () {
	//	// tua từ đầu rồi play để capture
	//	statePlay = 1;
	//	video.pause();
	//	video.currentTime = 0;
	//	video.play();		
	//	$("#btn-finish-upload").removeAttr('disabled');		
	//});

	//$("#btn-finish-upload").click(function () {				
	//	$("#btn-finish-upload").attr("disabled", "disabled");
	//	$("#upload-loading").addClass("fa fa-spinner fa-spin");
	//	stateUploading = 1;
	//	if (statePlay == 1) {
	//		App.DisplayError("Vui lòng chờ tải lên video!");		
	//		$("#btn-finish-upload").removeAttr('disabled');		
	//		$("#upload-loading").removeClass("fa fa-spinner fa-spin");
	//		stateUploading = 0;
	//		return;
	//	}
	//	if (base64Selected == undefined) {
	//		App.DisplayError("Vui lòng chọn anh chân dung");		
	//		$("#btn-finish-upload").removeAttr('disabled');		
	//		$("#upload-loading").removeClass("fa fa-spinner fa-spin");
	//		stateUploading = 0;
	//		return;
	//	}
	//	if (images == undefined || images.length < 20) {
	//		App.DisplayError("Vui lòng chọn video tải lên!");						
	//		$("#btn-finish-upload").removeAttr('disabled');		
	//		$("#upload-loading").removeClass("fa fa-spinner fa-spin");
	//		stateUploading = 0;
	//		return;
	//	}		
	//	base64Selected = base64Selected.replace("data:image/jpeg;base64,", "");
	//	for (var i = 0; i < images.length; i++) {
	//		images[i] = images[i].replace("data:image/jpeg;base64,", "");
	//	}
	//	var loanBriefId = parseInt($('#loanBriefId').val());
	//	var data = {
	//		selfie: base64Selected,
	//		files: images,
	//		loanBriefId: loanBriefId
	//	}

	//	$.ajax({
	//		type: "POST",
	//		url: "/Home/CheckLiveness",
	//		data: JSON.stringify(data),
	//		dataType: "json",
	//		contentType: "application/json; charset=utf-8"
	//	}).done(function (data) {
	//		if (data != undefined) {
	//			if (data.status == 1) {
	//				// Thành công chuyển sang màn tiếp theo
	//				var active = $('.wizard .nav-tabs li.active');
	//				active.next().removeClass('disabled');
	//				$(active).next().find('a[data-toggle="tab"]').click();
	//			}
	//			else if (data.status == -1) {
	//				// Thất bại vui lòng thực hiện lại
	//				App.DisplayError("Xác thực thất bại vui lòng quay lại video hoặc thay đổi ảnh chân dung!", 5000);
	//				$("#btn-finish-upload").removeAttr('disabled');
	//				$("#upload-loading").removeClass("fa fa-spinner fa-spin");
	//				stateUploading = 0;
	//			}
	//			else {
	//				App.DisplayError("Lỗi trong quá trình xử lý, vui lòng thực hiện lại sau.", 5000);
	//				$("#btn-finish-upload").removeAttr('disabled');
	//				$("#upload-loading").removeClass("fa fa-spinner fa-spin");
	//				stateUploading = 0;
	//			}
	//		}
	//		else {
	//			App.DisplayError("Lỗi trong quá trình xử lý, vui lòng thực hiện lại sau.", 5000);
	//			$("#btn-finish-upload").removeAttr('disabled');
	//			$("#upload-loading").removeClass("fa fa-spinner fa-spin");
	//			stateUploading = 0;
	//		}
	//	}).fail(function () {
	//		App.DisplayError("Lỗi trong quá trình xử lý, vui lòng thực hiện lại sau.", 5000);
	//		$("#btn-finish-capture").removeAttr('disabled');
	//		$("#capture-loading").removeClass("fa fa-spinner fa-spin");
	//		stateUploading = 0;
	//	});;
	//});
});

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$("#selfie-container").show();
			$('#selfie').attr('src', e.target.result).width(400)
			base64Selected = e.target.result;
		};
		reader.readAsDataURL(input.files[0]);
	}
}

function hasGetUserMedia() {
	return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);
}