﻿var LoginModule = new function () {
    var GetOtp = function (btn) {
        $(btn).attr('disabled', 'disabled');
        $(btn).attr('style', 'cursor: not-allowed;');
        $('#_btn_loading').addClass("fa fa-spinner fa-spin");
        var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
        var phone = $('#txt_phone').val();
        if (phone == "") {
            $('#_btn_loading').removeClass("fa fa-spinner fa-spin");
            $(btn).removeAttr('disabled', 'disabled');
            $(btn).removeAttr('style', 'cursor: not-allowed;');
            App.DisplayError("Vui lòng nhập số điện thoại.");
            return false;
        }
        else if (vnf_regex.test(phone.trim()) == false) {
            $('#_btn_loading').removeClass("fa fa-spinner fa-spin");
            $(btn).removeAttr('disabled', 'disabled');
            $(btn).removeAttr('style', 'cursor: not-allowed;');
            App.DisplayError("Số điện thoại không đúng định dạng.");
            return false;
        }
        $.ajax({
            type: "POST",
            url: '/Home/GetOtp',
            data: { phone: phone },
            success: function (data) {
                $('#_btn_loading').removeClass("fa fa-spinner fa-spin");
                $(btn).removeAttr('disabled', 'disabled');
                $(btn).removeAttr('style', 'cursor: not-allowed;');
                if (data.status == 1) {
                    window.location.href = "../otp.html";
                }
                else {
                    App.DisplayError(data.message);
                }
            }
        });

    }

    var VerifyOtp = function (btn) {
        $(btn).attr('disabled', 'disabled');
        $(btn).attr('style', 'cursor: not-allowed;');
        $('#_btn_loading').addClass("fa fa-spinner fa-spin");
        var otp = $('#txt_otp').val();
        $.ajax({
            type: "POST",
            url: '/Home/VerifyOtp',
            data: { otp: otp },
            success: function (data) {
                $('#_btn_loading').removeClass("fa fa-spinner fa-spin");
                $(btn).removeAttr('disabled', 'disabled');
                $(btn).removeAttr('style', 'cursor: not-allowed;');
                if (data.status == 1) {
                    //mà hình kế tiếp
                    window.location.href = data.url;

                }
                else if (data.status == -1) {
                    App.DisplayError(data.message);
                    setTimeout(function () { window.location.href = "/"; }, 2000);
                }
                else {
                    App.DisplayError(data.message);
                }
            }
        });
    }

    var CountdownReGetOTP = function (duration, display) {
        var timer = duration, minutes, seconds;
        var countDown = setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            display.textContent = minutes + ":" + seconds;
            if (--timer == 0) {
                $('#_timer').attr('style', 'display: none;');
                $('#btn_reget_otp').removeClass('btn-retype-opt-disabled');
                $('#btn_reget_otp').addClass('btn-retype-opt');
                $('#btn_reget_otp').removeAttr('disabled', 'disabled');
                clearInterval(countDown);
            }
        }, 1000);
    }

    var CaculatorTime = function () {
        var minutes = 60 * 10,
            display = document.querySelector('#txt_countdown_time');

        $('#_timer').removeAttr('style', 'display: none;');
        $('#btn_reget_otp').addClass('btn-retype-opt-disabled');
        $('#btn_reget_otp').removeClass('btn-retype-opt');
        $('#btn_reget_otp').attr('disabled', 'disabled');
        CountdownReGetOTP(minutes, display);
    }

    var ReGetOTP = function (phone) {

        CaculatorTime();

        $.ajax({
            type: "GET",
            url: "/Home/ReGetOtp",
            data: { phone: phone },
            success: function (data) {
                if (data.status == 1) {
                    App.DisplaySuccess(data.message);
                } else {
                    App.DisplayError(data.message);
                }
            }
        });
    }

    return {
        GetOtp: GetOtp,
        VerifyOtp: VerifyOtp,
        ReGetOTP: ReGetOTP,
        CaculatorTime: CaculatorTime
    }
}