﻿var UploadModule = new function () {

    var frontPicture = 0;
    var backPicture = 0;
    var frontImage;
    var backImage;

    const front = document.getElementById("front");
    const back = document.getElementById("back");

    var DisplayImage = function (event, type) {
        if (event.files && event.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //$("#selfie-container").show();
                $('#img-' + type).attr('src', e.target.result).width(200)
                if (type == 1)
                    cmndFront = event.files[0];
                else if (type == 2)
                    cmndBack = event.files[0];
            };
            reader.readAsDataURL(event.files[0]);
        }

    }

    var DisplayImage2 = function (event, type) {

        const constraints = {
            audio: false,
            video: true,
            video: { facingMode: { exact: 'environment' } }
        };
        $('#image-content').removeAttr('style', 'display: none;')

        navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
            if (type == 1) {
                $("#font-image-container").css("display", "block");
                $("#front-picture").css("display", "block");
                $("#re-front-picture").css("display", "none");

                $("#front-image").css("display", "none");
                $("#front").css("display", "block");
                if (backImage != undefined) {
                    $("#back-image").css("display", "block");
                    $("#back").css("display", "none");
                    $("#back-image").attr("src", backImage);
                }

                front.srcObject = stream;
            } else {
                $("#back-image").css("display", "none");
                $("#back").css("display", "block");

                $("#back-image-container").css("display", "block");
                $("#back-picture").css("display", "block");
                $("#re-back-picture").css("display", "none");
                if (frontImage != undefined) {
                    $("#front-image").css("display", "block");
                    $("#front").css("display", "none");
                    $("#front-image").attr("src", frontImage);
                }
                back.srcObject = stream;
            }

        }).catch(function () {
            App.DisplayError("Máy tính của bạn không được trang bị thiết bị quay video, vui lòng tải lên video!");
        });

    }

    var Picture = function (event, type) {
        if (type == 1) {
            $("#front-image").css("display", "none");
            $("#front").css("display", "block");
            if (backImage != undefined) {
                $("#back-image").css("display", "block");
                $("#back").css("display", "none");
                $("#back-image").attr("src", backImage);
            }

            front.pause();
            // Capture Image		
            var canvas = document.createElement("canvas")
            canvas.width = front.videoWidth;
            canvas.height = front.videoHeight;
            canvas.getContext("2d").drawImage(front, 0, 0);
            // Other browsers will fall back to image/png
            frontImage = canvas.toDataURL("image/jpeg");
            $("#front-picture").css("display", "none");
            $("#re-front-picture").css("display", "block");

        }
        else {
            $("#back-image").css("display", "none");
            $("#back").css("display", "block");
            if (frontImage != undefined) {
                $("#front-image").css("display", "block");
                $("#front").css("display", "none");
                $("#front-image").attr("src", frontImage);
            }

            back.pause();
            // Capture Image		
            var canvas2 = document.createElement("canvas")
            canvas2.width = back.videoWidth;
            canvas2.height = back.videoHeight;
            canvas2.getContext("2d").drawImage(back, 0, 0);
            // Other browsers will fall back to image/png
            backImage = canvas2.toDataURL("image/jpeg");
            $("#back-picture").css("display", "none");
            $("#re-back-picture").css("display", "block");

        }
    }

    var UploadFile = function (e) {

        if (frontImage == undefined || backImage == undefined) {
            App.DisplayError('Vui lòng upload đủ CMND mặt trước và sau');
            return;
        }
        frontImage = frontImage.replace("data:image/jpeg;base64,", "");
        backImage = backImage.replace("data:image/jpeg;base64,", "");
        $(e).attr('disabled', 'disabled');
        $(e).attr('style', 'cursor: not-allowed;');
        $('#load-step1').addClass("fa fa-spinner fa-spin");


        var documentId = $('#documentId').val();
        var loanBriefId = $('#loanBriefId').val();
        var fullName = $('#fullName').val();
        var nationalCard = $('#nationalCard').val();

        var formData = {
            documentId: documentId,
            loanBriefId: loanBriefId,
            fullName: fullName,
            nationalCard: nationalCard,
            frontFile: frontImage,
            backFile: backImage
        }


        $.ajax({
            type: "POST",
            url: "/Home/UploadFile2",
            data: formData,
            success: function (data) {
                $('#load-step1').removeClass("fa fa-spinner fa-spin");
                $(e).removeAttr('disabled', 'disabled');
                $(e).removeAttr('style', 'cursor: not-allowed;');
                if (data.status == 1) {
                    var typeRemarketing = $('#typeRemarketing').val();
                    if (typeRemarketing == 6 || typeRemarketing == 2)
                        window.location.href = '/loan-info.html';
                    else
                        nextStep();
                } else {
                    App.DisplayError(data.message);

                }
            }
        });
    }
    function nextStep() {
        var active = $('.wizard .nav-tabs li.active');
        active.next().removeClass('disabled');
        nextTab(active);
    }

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    return {
        DisplayImage: DisplayImage,
        UploadFile: UploadFile,
        DisplayImage: DisplayImage,
        DisplayImage2: DisplayImage2,
        Picture: Picture
    }
}