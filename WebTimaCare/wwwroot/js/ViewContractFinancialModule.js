﻿var ViewContractFinancialModel = new function () {
    var GetViewContractFinancial = function (btn, loanbriefId) {
        $(btn).attr('disabled', 'disabled');
        $(btn).attr('style', 'cursor: not-allowed;');
        $('#_btn_loading_' + loanbriefId).addClass("fa fa-spinner fa-spin");
        $.ajax({
            type: "POST",
            url: '/Home/GetViewContractFinancial',
            data: { loanbriefId: loanbriefId },
            success: function (data) {
                $('#_btn_loading_' + loanbriefId).removeClass("fa fa-spinner fa-spin");
                $(btn).removeAttr('disabled', 'disabled');
                $(btn).removeAttr('style', 'cursor: not-allowed;');
                if (data != null && data.data != null && data.data.data != null) {
                    let pdfWindow = window.open("")
                    pdfWindow.document.write(
                        "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
                        encodeURI(data.data.data) + "'></iframe>"
                    )
                }
                else {
                    App.DisplayError("Hợp đồng chưa tồn tại. Vui lòng thử lại sau.");
                }
            }
        });
    }

    return {
        GetViewContractFinancial: GetViewContractFinancial,
    }
}