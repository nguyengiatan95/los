#!/usr/bin/env bash
echo "============================================================"
echo "============================================================"
echo "================== LOS App Api  ===================="
echo "============================================================"
echo "============================================================"

echo "=========> Build image"
docker build -t registry.gitlab.com/developer109/los/appapi --file LOS.AppApi/Dockerfile .
docker tag registry.gitlab.com/developer109/los/appapi:latest registry.gitlab.com/developer109/los/appapi:prod.v1.0.27
docker push registry.gitlab.com/developer109/los/appapi:prod.v1.0.27

