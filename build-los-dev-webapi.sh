#!/usr/bin/env bash
echo "============================================================"
echo "============================================================"
echo "================== LOS Web Api  ===================="
echo "============================================================"
echo "============================================================"

echo "=========> Build image"
docker build -t registry.gitlab.com/developer109/los/webapi --file LOS.WebApi/Dockerfile .
docker tag registry.gitlab.com/developer109/los/webapi:latest registry.gitlab.com/developer109/los/webapi:prod.v1.0.28
docker push registry.gitlab.com/developer109/los/webapi:prod.v1.0.28
