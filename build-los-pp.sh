#!/usr/bin/env bash
echo "============================================================"
echo "============================================================"
echo "================== LOS Pipeline Processor  ===================="
echo "============================================================"
echo "============================================================"

echo "=========> Build image"
docker build -t registry.gitlab.com/developer109/los/pipelineprocessor --file LOS.PipelineProccesor/Dockerfile .
docker tag registry.gitlab.com/developer109/los/pipelineprocessor:latest registry.gitlab.com/developer109/los/pipelineprocessor:prod.v1.0.25
docker push registry.gitlab.com/developer109/los/pipelineprocessor:prod.v1.0.25

