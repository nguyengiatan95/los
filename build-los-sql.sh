#!/usr/bin/env bash
echo "============================================================"
echo "============================================================"
echo "================== LOS Pipeline Processor  ===================="
echo "============================================================"
echo "============================================================"

echo "=========> Build image"
docker build -t registry.gitlab.com/developer109/los/sqlsync --file LOS.SynchorizeSQLJob/Dockerfile .
docker tag registry.gitlab.com/developer109/los/sqlsync:latest registry.gitlab.com/developer109/los/sqlsync:prod.v1.0.0
docker push registry.gitlab.com/developer109/los/sqlsync:prod.v1.0.0

