#!/usr/bin/env bash
echo "============================================================"
echo "============================================================"
echo "================== LOS Third Party  ===================="
echo "============================================================"
echo "============================================================"

echo "=========> Build image"
docker build -t registry.gitlab.com/developer109/los/thirdparty --file LOS.ThirdPartyProccesor/Dockerfile .
docker tag registry.gitlab.com/developer109/los/thirdparty:latest registry.gitlab.com/developer109/los/thirdparty:prod.v1.0.29
docker push registry.gitlab.com/developer109/los/thirdparty:prod.v1.0.29