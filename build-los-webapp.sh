#!/usr/bin/env bash
echo "============================================================"
echo "============================================================"
echo "================== LOS Web App  ===================="
echo "============================================================"
echo "============================================================"

echo "=========> Build image"
docker build -t registry.gitlab.com/developer109/los/webapp --file LOS.WebApp/Dockerfile .
docker tag registry.gitlab.com/developer109/los/webapp:latest registry.gitlab.com/developer109/los/webapp:latest
docker push registry.gitlab.com/developer109/los/webapp:latest

