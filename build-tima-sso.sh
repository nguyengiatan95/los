#!/usr/bin/env bash
echo "============================================================"
echo "============================================================"
echo "================== Tima SSO  ===================="
echo "============================================================"
echo "============================================================"

echo "=========> Build image"
docker build -t registry.gitlab.com/developer109/los/sso --file SSO/Dockerfile .
docker tag registry.gitlab.com/developer109/los/sso:latest registry.gitlab.com/developer109/los/sso:prod.v1.0.5
docker push registry.gitlab.com/developer109/los/sso:prod.v1.0.5

